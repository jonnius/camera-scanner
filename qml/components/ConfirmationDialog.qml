import QtQuick 2.6
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

Dialog {
    id: confirmationDialog

    property alias confirmationDialogText: confirmationDialog.title
    property alias descriptionLabelText: descriptionLabel.text
    property alias confirmButtonText: confirmButton.text
    property alias cancelButtonText: cancelButton.text
    property bool confirmIsDestructive

    signal confirmClicked
    signal cancelClicked

    Label {
        id: descriptionLabel
        horizontalAlignment: Text.AlignHCenter
        wrapMode: Text.WordWrap
    }

    Row {
        anchors {
            left: parent.left
            right: parent.right
        }
        spacing: units.gu(1)

        Button {
            id: cancelButton
            width: parent.width / 2 - units.gu(0.5)
            onClicked: {
                cancelClicked()
                PopupUtils.close(confirmationDialog)
            }
        }
        Button {
            id: confirmButton
            color: confirmIsDestructive ? theme.palette.normal.negative : theme.palette.normal.focus
            width: parent.width / 2 - units.gu(0.5)
            onClicked: {
                confirmClicked()
                PopupUtils.close(confirmationDialog)
            }
        }
    }
}
